# Database Meta #

```
export DBMETA_FILE_PATH={path for database files}
java -jar db-meta-0.1.jar
```

### What is this repository for? ###

* **db-meta** - A Java Servlet based program for serving Database Meta information.
* v0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* @owner: Yoon Moon 
* @email: yoonmoon.nz@gmail.com