import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import xyz.ymoon.Config;

@WebServlet("/dbjson")
public class dbJson extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException {

    ServletContext context = request.getServletContext();
    String env1 = System.getenv("USER");
    String configFile = System.getenv("DBMETA_CONFIG");
    //response.setContentType("text/html;");
    response.setContentType("application/json");
    PrintWriter out = response.getWriter();
    System.out.println("configFile: "+configFile);
    JSONObject json = Config.parseJSONFile(configFile);
    out.print(json.toString());
  }
}
