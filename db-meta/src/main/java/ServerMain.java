import java.net.URL;
import org.eclipse.jetty.server.Server;
//import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.plus.webapp.EnvConfiguration;
import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.webapp.*;

/** Starts up a server that serves static files and annotated servlets. */
public class ServerMain {

  public static void main(String[] args) throws Exception {

    // Create a server that listens on port 8080.
    Server server = new Server(8080);
    WebAppContext webAppContext = new WebAppContext();
    
    webAppContext.setContextPath("/");

    // Configuration classes. This gives support for multiple features.
    // The annotationConfiguration is required to support annotations like @WebServlet
    webAppContext.setConfigurations(new Configuration[] {
      new AnnotationConfiguration(), new WebXmlConfiguration(),
      new WebInfConfiguration(),
      new PlusConfiguration(), new MetaInfConfiguration(),
      new FragmentConfiguration(), new EnvConfiguration() });


    // Load static content from inside the jar file.
    URL webAppDir =
        ServerMain.class.getClassLoader().getResource("META-INF/resources");
    webAppContext.setResourceBase(webAppDir.toURI().toString());

    // Look for annotations in the classes directory (dev server) and in the
    // jar file (live server)
    webAppContext.setAttribute(
        "org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern", 
        ".*/target/classes/|.*\\.jar");

    webAppContext.setParentLoaderPriority(true);

    // Start the server!
    server.setHandler(webAppContext);
    server.start();
    System.out.println("Server started!");
    //server.dump(System.err);
    // Keep the main thread alive while the server is running.
    server.join();
  }
}