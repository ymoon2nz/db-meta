import java.util.Date;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

@WebServlet("/download")
public class DownloadServlet extends HttpServlet {
    private final int ARBITARY_SIZE = 1048;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
      throws ServletException, IOException {

        String envPath = System.getenv("DBMETA_FILE_PATH");
        String path = req.getParameter("path");
        String file = req.getParameter("file");
        String pathNfile = "";

        if (envPath == null || envPath.equals("")) {   // envPath not set
            System.out.println(new Date()+"-downloading: DBMETA_FILE_PATH environment parameter is not set!");
        } else if (file == null || file.equals("")) {
            System.out.println(new Date()+"-downloading: file parameter is not set!");
        } else {
            if (path == null || path.equals("")) {
                pathNfile = envPath+"/"+file;
            } else {
                pathNfile = envPath+"/"+path+"/"+file;
            }
            // gets MIME type of the file
            String mimeType = req.getServletContext().getMimeType(file);
            if (mimeType == null) {        
                // set to binary type if MIME mapping not found
                mimeType = "application/octet-stream";
            }
            System.out.println(new Date()+"-downloading: "+file+", "+"MIME type: " + mimeType);
        
            resp.setContentType(mimeType);
            resp.setHeader("Content-disposition", "attachment; filename="+file);

            //try(InputStream in = req.getServletContext().getResourceAsStream(filePath);
            try(FileInputStream in = new FileInputStream(pathNfile);
                OutputStream out = resp.getOutputStream();
                ) {
                byte[] buffer = new byte[ARBITARY_SIZE];
            
                int numBytesRead;
                while ((numBytesRead = in.read(buffer)) > 0) {
                    out.write(buffer, 0, numBytesRead);
                }
                
                in.close();
                out.flush();
            }
        }
    }
}