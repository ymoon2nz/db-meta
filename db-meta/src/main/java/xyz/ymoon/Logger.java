package xyz.ymoon;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class Logger {
	
	private final static int LEVEL_MAX = 5;
	private final static int LEVEL_DEFAULT = 4;
	//private Object current = "null";
	private int logLevel = 4;	// 1 ~ 5, 1 is highest
	private int logDetail = 0;
	private HttpServletRequest request=null;
	
	public Logger(HttpServletRequest request) {
		this(LEVEL_DEFAULT, request);
	}
	public Logger(int logDetail, HttpServletRequest request) {
		this.logDetail = logDetail;
		this.request = request;
		String reqLogLevel = request.getParameter("loglevel");
		try {
			this.logLevel = Integer.parseInt(reqLogLevel);
			System.out.println("+setLogLevel="+this.logLevel);
		} catch (Exception e) {}		
	}
	
	public void print(int logLevel, String message) {
		print(logLevel, message, null);
	}
	
	public void print(String message, Object current) {
		print(LEVEL_DEFAULT, message, current);
	}

	public void print(int logDetail, String message,  Object current) {
		if (message==null) message = "";
		if ( logDetail >= logLevel ) {
			String prefix = "";
			for (int i=logDetail; i < LEVEL_MAX; i++) {
				prefix +=".";
			}
			
			String currentName = ".";
			if ( logDetail>=5 ) {
				try { currentName = current.getClass().getName(); } catch (Exception e) {}
				System.out.println(prefix+(new Date()).toString()+" ["+currentName+"] +sessionId="+request.getSession().getId()+"@"+request.getRemoteAddr()+prefix+message.toString());
			} else if( logDetail>=4 ) {
				try { currentName = current.getClass().getName(); } catch (Exception e) {}
				System.out.println(prefix+(new Date()).toString()+" ["+currentName+"]"+message.toString());
			} else if( logDetail>=3 ) {
				System.out.println(prefix+(new Date()).toString()+" "+message.toString());
			} else if( logDetail>=2 ) {
				System.out.println(prefix+message.toString());
			} else if( logDetail>=1 ) {
				System.out.println(prefix+message.toString());
			}
		}
	}
	
	public void printSession() {
		if (request==null) {
			System.out.println(" "+(new Date()).toString()+" "+Thread.currentThread().getStackTrace()[1].getClassName()+" No session assigned with Logger.");
		} else {
			System.out.println(" "+(new Date()).toString()+" "+Thread.currentThread().getStackTrace()[1].getClassName()+" +sessionId="+request.getSession().getId()+"@"+request.getRemoteAddr());
		}		
	}

	private int getLogLevel() {
		return logLevel;
	}

	private void setLogLevel(int setLevel) {
		this.logLevel = setLevel;
	}
	private int getLogDetail() {
		return logDetail;
	}
	private void setLogDetail(int logDetail) {
		this.logDetail = logDetail;
	}
}
