package xyz.ymoon.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

//import oracle.jdbc.driver.OracleConnection;
import xyz.ymoon.security.AES256Helper;

/**
 * Database Connection Manager for JDBC connections.
 * This program create different type of a vendor Database connection for JDBC base on url.
 * 
 * @author	Yoon Moon
 * @version	0.1
 *
 */
public class JdbcConnManager {

	static String URL_DELIMITER = " ";
	static int DEFAULT_ISOLATION = Connection.TRANSACTION_READ_UNCOMMITTED; 
	

	public static Connection getConnectionByEncpassword(String url_n_credential, String decryptKey) {
		
		String[] urlArr = url_n_credential.split(URL_DELIMITER);
		return getConnection(urlArr[0], urlArr[1], AES256Helper.decrypt(urlArr[2], decryptKey) );
	}
	
	public static Connection getConnection2ByEncpassword(String url_n_credential, String decryptKey) throws ClassNotFoundException, SQLException {
		
		String[] urlArr = url_n_credential.split(URL_DELIMITER);
		return getConnection2(urlArr[0], urlArr[1], AES256Helper.decrypt(urlArr[2], decryptKey) );
	}
	
	public static Connection getConnection(String url_n_credential) {
		
		String[] urlArr = url_n_credential.split(URL_DELIMITER);
		return getConnection(urlArr[0], urlArr[1], urlArr[2]);
	}
	
	public static Connection getConnection2(String url, String user, String password) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		// MS SQL server JDBC driver
		if (url.contains("sqlserver")) {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			conn = DriverManager.getConnection(url, user, password);
			conn.setTransactionIsolation(DEFAULT_ISOLATION);
			conn.setReadOnly(true);

			System.out.println((new Date()).toString()+" - Connection error = sqlUrl[0]");


		} else if (url.contains("jdbc:db2")) {			

			Class.forName("com.ibm.db2.jcc.DB2Driver");
			Properties prop = new Properties();
			prop.put("user", user);
			prop.put("password", password);
							
			conn = DriverManager.getConnection(url, prop);
			conn.setTransactionIsolation(DEFAULT_ISOLATION);
			//conn.setReadOnly(true);

		} else if (url.contains("jdbc:postgres")) {
			try {
				Class.forName("org.postgresql.Driver");
				Properties prop = new Properties();
				prop.setProperty("user", user);
				prop.setProperty("password", password);
				conn = DriverManager.getConnection(url, prop);
				conn.setReadOnly(true);
			} catch (ClassNotFoundException cnfe) {
				cnfe.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		/** } else if (url.contains("jdbc:oracle")) {			

			DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
			//Class.forName("oracle.jdbc.driver.OracleDriver");
			Properties props = new Properties();
			props.setProperty("user", user);
			props.setProperty("password", password);
			props.setProperty(OracleConnection.CONNECTION_PROPERTY_THIN_NET_CONNECT_TIMEOUT, "12000");
			conn = DriverManager.getConnection(url, props);			
			//conn.setTransactionIsolation(DEFAULT_ISOLATION);
			//conn.setReadOnly(true);
		**/
		} else {
			try {
				throw new ClassNotFoundException("Can't find class for url : "+url);
			} catch (Exception e) {e.printStackTrace();}
		}
		return conn;
	}

	public static Connection getConnection(String url, String user, String password) {
		Connection conn = null;
		// MS SQL server JDBC driver
		if (url.contains("sqlserver")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			} catch (ClassNotFoundException cnfe) {
				try {
					throw new SQLException("Can't find class for driver: com.microsoft.sqlserver.jdbc.SQLServerDriver");
				} catch (SQLException e) {e.printStackTrace();}
			}

			try {
				conn = DriverManager.getConnection(url, user, password);
				conn.setTransactionIsolation(DEFAULT_ISOLATION);
				//conn.setReadOnly(true);
			} catch (SQLException e) {
				System.out.println((new Date()).toString()+" - Connection error = sqlUrl[0]");
				e.printStackTrace();
			}

		} else if (url.contains("jdbc:db2")) {			
			try {
				Class.forName("com.ibm.db2.jcc.DB2Driver");
				Properties prop = new Properties();
				prop.put("user", user);
				prop.put("password", password);
								
				conn = DriverManager.getConnection(url, prop);
				conn.setTransactionIsolation(DEFAULT_ISOLATION);
				//conn.setReadOnly(true);
			} catch (ClassNotFoundException cnfe) {
				try {
					throw new SQLException("Can't find class for driver: com.ibm.db2.jcc.DB2Driver");
				} catch (SQLException e) {e.printStackTrace();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else if (url.contains("jdbc:postgres")) {
			try {
				Class.forName("org.postgresql.Driver");
				Properties prop = new Properties();
				prop.setProperty("user", user);
				prop.setProperty("password", password);
				conn = DriverManager.getConnection(url, prop);
				conn.setReadOnly(true);
			} catch (ClassNotFoundException cnfe) {
				cnfe.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		/**} else if (url.contains("jdbc:oracle")) {			

			try {
				//DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
				Class.forName("oracle.jdbc.driver.OracleDriver");
				Properties props = new Properties();
				props.setProperty("user", user);
				props.setProperty("password", password);
				props.setProperty(OracleConnection.CONNECTION_PROPERTY_THIN_NET_CONNECT_TIMEOUT, "12000");
				conn = DriverManager.getConnection(url, props);
				//conn.setTransactionIsolation(DEFAULT_ISOLATION);
				//conn.setReadOnly(true);
			} catch (ClassNotFoundException cnfe) {
				cnfe.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		**/					
		} else {
			try {
				throw new ClassNotFoundException("Can't find class for url : "+url);
			} catch (Exception e) {e.printStackTrace();}
		}
		return conn;
	}
}
