package xyz.ymoon.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

public class Db4Json {

	public static JSONObject getResultSetInColumnArray (Connection conn, String sql) {
		
		JSONObject json = new JSONObject();
		try {

			ResultSet rs = conn.createStatement().executeQuery(sql);
			int colCount = rs.getMetaData().getColumnCount();
			JSONArray[] jsonArr = new JSONArray[colCount];
			String[] colNames = new String[colCount];
			for (int i=0; i < colCount; i++) {
				jsonArr[i] = new JSONArray();
				colNames[i] = rs.getMetaData().getColumnName(1+i);
			}
			while (rs.next()) {
				for (int i=0; i < colCount; i++) {
					jsonArr[i].put( rs.getObject(1+i) );
				}
			}
			for (int i=0; i < colCount; i++) {
				json.put(colNames[i].toLowerCase(), jsonArr[i]);
			}
			rs.close();
			json.put("url", conn.getMetaData().getURL());
			json.put("product", conn.getMetaData().getDatabaseProductName());
		} catch (SQLException e) {
			e.printStackTrace();
			json.put("returnCode", "error");
			json.put("errorMessage", "getJsonResultSetByColumnArray() encounter error");
		}
		//System.out.println( " getJsonResultSetByColumnArray : "+json);
		return json;
		
	}
}
