package xyz.ymoon.db;

import java.util.Properties;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnManager {

	Connection conn;
	String url;
	String user;
	String password;

	public ConnManager(String url, String user, String password) {
		this.url = url;
		this.user = user;
		this.password = password;
	}

	public Connection getConnection() {
		Connection conn = null;
		
		// LOAD sqlserver JDBC driver
		if (url.contains("sqlserver")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			} catch (ClassNotFoundException cnfe) {

				try {
					throw new SQLException("Can't find class for driver: ");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//System.out.println("DB url := " + url);
			//System.out.println("Trying with following : " + url);
			try {
				conn = DriverManager.getConnection(url);
				conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
				conn.setReadOnly(true);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (url.contains("jdbc:postgres")) {
			try {
				Class.forName("org.postgresql.Driver");
				Properties props = new Properties();
				props.setProperty("user", user);
				props.setProperty("password", password);
				conn = DriverManager.getConnection(url, props);
				conn.setReadOnly(true);
			} catch (ClassNotFoundException cnfe) {
				cnfe.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}	
		} else if (url.contains("jdbc:db2")) {
			//String[] db2url = url.split(" ");
			
			try {
				//System.out.println("Loading DB2 JDBC driver: com.ibm.db2.jcc.DB2Driver");
				Class.forName("com.ibm.db2.jcc.DB2Driver");
				//System.out.println("DB url := " + url);
				//System.out.println("Trying with following : " + url);
				conn = DriverManager.getConnection(url, user, password);
				conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
				conn.setReadOnly(true);
			} catch (ClassNotFoundException cnfe) {
				cnfe.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		} else {
			try {
				conn = DriverManager.getConnection(url);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return conn;
	}
}
