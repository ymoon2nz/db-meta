package xyz.ymoon.db;

public class SqlBuilder {
	
	public static String applyWhereSQL(String sql, String where, int level) {
		String newSql = "SELECT * FROM ("+sql+") t"+level+" WHERE "+where;
		System.out.println("SqlBuilder - newSql: "+newSql);
		return newSql;
	}
	
	public static String applySearchClause(String sql, String column, String searchString) {
		
		return applySearchClause(sql, column, searchString, 0);
		
	}
	public static String applySearchClause(String sql, String column, String searchString, int level) {
		
		String newSearchString = searchString.replace('*', '%');
		return applyWhereSQL(sql, "UPPER("+column+") like UPPER('%"+newSearchString+"%')", level);
	}
	
	public static String applyOrderBySQL(String sql, String orderColumnString) {
		
		return sql + " ORDER BY "+orderColumnString;
	}
}
