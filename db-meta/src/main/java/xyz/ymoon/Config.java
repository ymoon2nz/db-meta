package xyz.ymoon;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class Config {
	public static final long serialVersionUID = 1L;
	public static final String PATH 	= "/WEB-INF/";
	public static final String CFGFILE	= "DBMETA_FILE";

	public static Properties getProperties(Logger l, HttpServletRequest request, String filename) {
		Properties prop;
		try {
			prop = new Properties();
			l.print(2, "Config.getProperties : "+filename);
			prop.load(request.getServletContext().getResourceAsStream(filename));
			return prop;
		} catch (Exception e) {
			l.print(3, "Config.getProperties : +passwordfilename="+filename);
			prop = null;
		}
		return prop;
	}
	
	public static Properties getCacheProp(Logger l, HttpServletRequest request, String filename) {
		
		ServletContext context = request.getServletContext();
		Properties prop = null;
		l.print(2, "Config.getCacheProp : "+filename);
		try {
			prop = (Properties) context.getAttribute("Properties."+filename);
			//l.print(1, "getCacheProp : Properties="+prop.toString());
		} catch (Exception e) {
			l.print(4, "Config.getCacheProp() - Error while getting : +filename="+filename);
			prop = null;
		}
		// Not Found in cache... Sync read from file
		if ( prop == null ) {
			prop = getProperties(l, request, filename);
			if ( prop != null) {
				context.setAttribute("Properties."+filename, prop);
			}
		}
		return prop;		
	}
	
	public static String getCacheCfgParam(Logger l, HttpServletRequest request, String paramName) {
		Properties cfgProp = getCacheProp(l, request, CFGFILE);
		String param = cfgProp.getProperty(paramName);
		l.print(2, "getCacheCfgParam("+paramName+") : "+param);
		return param;
		
	}
	
	public static JSONObject parseJSONFile(String filename) throws JSONException, IOException {
        String content = new String(Files.readAllBytes(Paths.get(filename)));
        return new JSONObject(content);
    }
	
	public static JSONObject getJson(Logger l, HttpServletRequest request, String filename) {
		JSONObject json = null;
		// Read from JSON file
		try {
			json = parseJSONFile(request.getSession().getServletContext().getRealPath(filename));
		} catch (Exception e) {
			l.print(4, "Config.getJson() - Error while getting : +filename="+filename);
			e.printStackTrace();
			json = null;
		}
		return json;
	}
	
	public static JSONObject getCacheJson(Logger l, HttpServletRequest request, String filename) {
		ServletContext context = request.getServletContext();
		JSONObject json = null;
		l.print(2, "Config.getCacheJson : "+filename);
		try {
			json = (JSONObject) context.getAttribute("Json."+filename);
			//l.print(1, "getCacheJson : json="+json.toString());
		} catch (Exception e) {
			l.print(4, "Config.getCacheJson() - Error while getting : +filename="+filename);
			json = null;
		}
		// Added to Context as cache
		if ( json ==null ) {
			json = getJson(l, request, filename);
			if (json!=null) {
				context.setAttribute("Json."+filename, json);
			}
		}
		return json;	
	}
	
}
