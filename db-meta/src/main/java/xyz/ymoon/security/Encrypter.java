package xyz.ymoon.security;

import java.io.IOException;
import java.util.Scanner;

public class Encrypter {

	public static void main(String[] args) throws IOException {
		
		Scanner user_input = new Scanner( System.in );
		String str2enc;
		System.out.print("Enter String to Encrypt: ");
		str2enc = user_input.next( );
		
		String key;
		System.out.print("Enter Encryption Key: ");
		key = user_input.next( );

		// Printing the read line
		System.out.println("String to Encyrpt	: "+str2enc);
		System.out.println("Encrypt Key 		: "+key);
		System.out.println("Encrypted String	: "+xyz.ymoon.security.AES256Helper.encrypt(str2enc, key));
	}
}
